elfrc (0.7-5) unstable; urgency=medium

  * QA upload.
  * debian/copyright: Update packaging copyright information.
  * debian/salsa-ci.yml: Add to provide CI tests for Salsa.
  * debian/tests/control: Create autopkgtest.

 -- Mauro Nadalin <mnadalin@alunos.utfpr.edu.br>  Thu, 28 Nov 2024 02:53:21 -0300

elfrc (0.7-4) unstable; urgency=medium

  * QA upload.
  * debian/: Apply "wrap-and-sort -abst".
  * debian/control: Bump Standards-Version to 4.6.2.
  * debian/control: Move Vcs-* to Debian Salsa.

 -- Boyuan Yang <byang@debian.org>  Thu, 16 Feb 2023 15:37:00 -0500

elfrc (0.7-3) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards-Version to 4.5.0.

 -- Bruno Naibert de Campos <bruno.naibert@gmail.com>  Sat, 16 May 2020 23:29:14 -0300

elfrc (0.7-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #678874)
  * DH level to 9.
  * DebSrc to version 3.0.
  * debian/control:
     - Bumped Standards-Version to 3.9.6.
     - Removed link to homepage, url address is broken.
     - Improved organization of long description.
  * debian/copyright:
     - Migrated to 1.0 format.
     - Revised and updated all information.
  * debian/install:
     - install the elfrc binary to right place.
  * debian/patches:
      - Added patch to fix the Makefile for hardening.
  * debian/rules:
      - Updated to reduced format.
      - Included DEB_BUILD_MAINT_OPTIONS for hardening.
  * debian/watch:
      - Updated because old url address is broken.
      - Added a fake site to explain about the current status
        of the original upstream homepage.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 26 Sep 2015 20:33:33 -0300

elfrc (0.7-1) unstable; urgency=low

  * Initial release (Closes: #432964)

 -- Krzysztof Burghardt <krzysztof@burghardt.pl>  Thu, 18 Sep 2008 20:18:01 +0200
